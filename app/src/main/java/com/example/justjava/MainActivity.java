package com.example.justjava;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {
    private int quantity = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This method is called when the order button is clicked
     * and opens a e-mail app with the summary of your order
     */
    public void submitOrder(View view) {
        CheckBox checkBox = findViewById(R.id.ch_whipped_cream);
        boolean hasWhippedCream = checkBox.isChecked();

        CheckBox checkBox1 = findViewById(R.id.ch_chocolate);
        boolean hasChocolate = checkBox1.isChecked();

        EditText nameField = findViewById(R.id.name_field);
        String name = nameField.getText().toString();

        int price = calculatePrice(hasWhippedCream, hasChocolate);
        String priceMessage = createOrderSummary(price, hasWhippedCream, hasChocolate, name);

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.just_java_order) + " " + name);
        intent.putExtra(Intent.EXTRA_TEXT, priceMessage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

    }
    /**
     * This method calculates the price of the coffee per cup.
     *
     * @return the total price.
     */

    private int calculatePrice(boolean addWhippedCream, boolean addChocolate){
        int basePrice = 5;
        if(addChocolate){
            basePrice = basePrice + 2;
        }
        if (addWhippedCream){
            basePrice = basePrice + 1;
        }
        return quantity * basePrice;
    }

    /**
     * This method makes a summary of the order
     *
     * @param price             on the order
     * @param addWhippedCream   is whether or not to add whipped cream to the coffee
     * @param addChocolate      is whether or not to add whipped cream to the coffee
     * @param name              on the order
     * @return text summary
     */
    private String createOrderSummary(int price, boolean addWhippedCream, boolean addChocolate, String name){
        String priceMessage;
        priceMessage = getString(R.string.order_summary_name, name);
        priceMessage += "\n" + getString(R.string.add_whipped_cream)  + "? " + translateBoolean(addWhippedCream);
        priceMessage += "\n" + getString(R.string.add_chocolate) + "? " + translateBoolean(addChocolate);
        priceMessage += "\n" + getString(R.string.quantity) + ":" + quantity;
        priceMessage += "\n" + getString(R.string.total) + ": $" + price;
        priceMessage += "\n" + getString(R.string.thank_you);
        return priceMessage;
    }
    /**
     * This method displays the given quantity value on the screen.
     */
    private void displayQuantity(int number) {
        TextView quantityTextView = findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }

    /**
    * This method is called when the plus button is clicked.
     */
    public void increment(View view){
        if(quantity == 100){
            Toast.makeText(this, getString(R.string.to_much), Toast.LENGTH_LONG).show();
            return;
        }
        else quantity++;
        displayQuantity(quantity);

    }
    /**
     * This method is called when the minus button is clicked.
     */
    public void decrement(View view){
        if (quantity == 1){
            Toast.makeText(this, getString(R.string.to_low), Toast.LENGTH_LONG).show();
            return;
        }
        else quantity--;
        displayQuantity(quantity);
    }

    /**
     * This method changes the boolean value into a yes/no.
     *
     * @param b    boolean parameter
     *
     * @return the translation.
     */
    private String translateBoolean(boolean b) {
        if(!b){
            return getString(R.string.no);
        } else return getString(R.string.yes);
    }

}